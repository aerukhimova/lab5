#pragma once
#include <stdbool.h>
//элемент листа
typedef struct node {
    int data;
    struct node *next;
} node;


//создает новый элемент
node *list_create(int data, node *next);

//добавляет элемент в начало
node *list_add_front(node *pointer, int data);

//добавляет элемент в конец
node *list_add_back(node *pointer, int data);

//выводит лист
void list_show(node *i);

node *list_free(node *nod);

//подсчет количества элемнетов
int list_length(node *pointer);

//сумма элементов
int list_sum(node *i);

//достаёт значение элемента по индексу
node *list_get(node *pointer, int index);

void list_set(node *pointer, int index, int data);

void foreach(node *pointer, void (*func)(int));

node *map(int (*func)(int), node *pointer);

void map_mut(int (*func)(int), node *pointer);

int foldl(int rax, int (*func)(int, int), node *start);

node *iterate(int s, int n, int (*func)(int));

bool save(node *head, const char *filename);

bool load(node **head, const char *filename);

bool serialize(node *head, const char *filename);

bool deserialize(node **head, const char *filename);



