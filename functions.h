#pragma once

#include <limits.h>
#include <stdio.h>


void print_value(int x);

void print_value_ln(int x);

int square(int x);

int cube(int x);

int sum(int a, int b);

int max(int a, int b);

int min(int a, int b);

int abs(int a);

int mul2(int a);